package wegood.cdk;

import software.amazon.awscdk.core.Duration;
import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.services.ec2.Port;
import software.amazon.awscdk.services.ecs.Cluster;
import software.amazon.awscdk.services.ecs.ScalableTaskCount;
import software.amazon.awscdk.services.ecs.FargateService;
import software.amazon.awscdk.services.ecs.ContainerImage;
import software.amazon.awscdk.services.ecs.CloudMapOptions;
import software.amazon.awscdk.services.ecs.ContainerDefinitionOptions;
import software.amazon.awscdk.services.ecs.RequestCountScalingProps;
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedFargateService;
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedTaskImageOptions;
import software.amazon.awscdk.services.elasticloadbalancingv2.ApplicationTargetGroup;
import software.amazon.awscdk.services.servicediscovery.PrivateDnsNamespace;
import software.amazon.awscdk.services.applicationautoscaling.EnableScalingProps;

import java.util.Map;
import java.util.HashMap;

public class WeGood {

	private ApplicationLoadBalancedFargateService albfs;

    public WeGood(final Stack stack, final Cluster cluster, final String id,
		String serviceName, PrivateDnsNamespace privateDnsNamespace) {

		/* Create a load-balanced Fargate service for the wegood microservice and make it public */
		this.albfs = ApplicationLoadBalancedFargateService.Builder.create(stack, id)
			.serviceName(serviceName)
			.cluster(cluster)           // Required
			.cpu(512)                   // Default is 256
			.desiredCount(2)            // Default is 1
			.cloudMapOptions(CloudMapOptions.builder() // auto discovery mode
				.cloudMapNamespace(privateDnsNamespace)
				.name("wegood")
				.build())
			.taskImageOptions(	// adding wegood microservice from ECR repo
				ApplicationLoadBalancedTaskImageOptions.builder()
				.image(ContainerImage.fromRegistry("cliffberg/wegood"))
				.containerPort(4567)
				.containerName("wegood")
				.build())
			.memoryLimitMiB(1024)       // Default is 512
			.publicLoadBalancer(true)   // Default is false
			.build();

		ApplicationTargetGroup applicationWeGoodTargetGroup = albfs.getTargetGroup();

		/* Set Autoscaling property on a service with max of 3 contianers and minimum of 1.
		If these are not set, the minimum capacity will be zero, and the first request
		to the service will fail.
		*/

		FargateService fargateService_wegood = albfs.getService();

		ScalableTaskCount scalecount_wegood =
			fargateService_wegood.autoScaleTaskCount(EnableScalingProps.builder()
			.maxCapacity(3)
			.minCapacity(1)
			.build());

		scalecount_wegood.scaleOnRequestCount("scaling_wegood",
			RequestCountScalingProps.builder()
			.policyName("requestCount_wegood")
			.requestsPerTarget(10)
			.scaleInCooldown(Duration.seconds(300))
			.scaleOutCooldown(Duration.seconds(300))
			.targetGroup(applicationWeGoodTargetGroup)
			.build());
    }

	public ApplicationLoadBalancedFargateService getALBFS() { return albfs; }
}
