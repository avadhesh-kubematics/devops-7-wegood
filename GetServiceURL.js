// Parse the output of this AWS command:
//	aws cloudformation describe-stacks --stack-name HelloTestStack --output json
// to retrieve the name of the service.

var awsoutput=process.argv[2]
var ar = JSON.parse(awsoutput).Stacks[0].Outputs;
var len = ar.length;
if (len == 0) throw "Stack not found";
var loadbal = null;
for (i = 0; i < len; i++) {
	if (ar[i].OutputKey.includes('wegoodserviceServiceURL')) {
		loadbal = ar[i].OutputValue;
		break;
	}
}
if (loadbal == null) {
	throw "Load balancer not found";
} else {
	console.log(loadbal);
}
