package wegood.stub;
public class WeGoodProxyFactory implements WeGoodFactory {
    public WeGoodStub createWeGood() throws Exception {
        return new WeGoodProxy();
    }
}
