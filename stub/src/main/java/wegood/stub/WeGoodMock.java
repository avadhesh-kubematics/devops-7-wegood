package wegood.stub;

import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class WeGoodMock implements WeGoodStub {

	private String timeFilePath = System.getenv("WEGOOD_MOCK_TIME_FILE_PATH");
	private File file;

	public WeGoodMock() throws Exception {
		if (timeFilePath == null) throw new RuntimeException("WEGOOD_MOCK_TIME_FILE_PATH is undefined");
		this.file = new File(timeFilePath);
		this.file.createNewFile(); // only creates it if the file does not exist.
	}

	public boolean areWeGood() throws Exception {
		LocalTime time = getTime();
		int hourOfDay = time.get(ChronoField.HOUR_OF_DAY);
		return ((hourOfDay >= 18) || (hourOfDay < 9));
	}

	private LocalTime time = null;

	public void setTime(LocalTime time) throws Exception {
		/* Write the time to the time file. */
		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(time);
	}

	public LocalTime getTime() throws Exception {
		/* Read the time from the time file. */
		FileInputStream fis = new FileInputStream(file);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Object obj = ois.readObject();
		LocalTime time = (LocalTime)obj;
		return time;
	}

	public String getServiceURL() {
		return System.getenv("WEGOOD_SERVICE_URL");
	}
}
