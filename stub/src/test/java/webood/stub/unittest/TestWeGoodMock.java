package wegood.stub.unittest;

import wegood.stub.WeGoodMock;
import wegood.stub.WeGoodStub;
import wegood.stub.WeGoodMockFactory;
import java.time.LocalTime;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestWeGoodMock {

	@Test
	public void testMock() throws Exception {
		WeGoodMockFactory factory = new WeGoodMockFactory();
		WeGoodStub stub = factory.createWeGood();  // should be a WeGoodMock
		assertTrue(stub instanceof WeGoodMock);
		WeGoodMock mock = (WeGoodMock)stub;
		LocalTime timeSet = LocalTime.of(18, 1);
		mock.setTime(timeSet);
		LocalTime timeRead = mock.getTime();
		assertEquals(timeRead, timeSet);
	}
}
